﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    private const float ROTATION_SPEED = 0.2f;

    private const float MOVEMENT_SPEED = 8.0f;

    private Rigidbody2D _rigidbody;

    public void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    public void FixedUpdate()
    {
        Player player = FindObjectOfType<Player>();
        if (player != null && !GameState.isCurrentRoomInstanceCompleted) {
            Transform target = player.transform;

            // Compute angle towards player
            float playerAngle = Vector2.SignedAngle(Vector2.up, target.position - transform.position);
            float deltaAngle = playerAngle - _rigidbody.rotation;
            if (deltaAngle > 180) deltaAngle -= 360;
            if (deltaAngle < -180) deltaAngle += 360;

            // Rotate towards player
            if (Mathf.Abs(deltaAngle) > 180)
            {
                if (playerAngle > _rigidbody.rotation) _rigidbody.rotation += 360;
                else _rigidbody.rotation -= 360;
            }
            _rigidbody.rotation += deltaAngle * ROTATION_SPEED;

            // Move forward, slower if we're not in the right direction
            Vector2 direction = Quaternion.AngleAxis(_rigidbody.rotation, Vector3.forward) * Vector2.up;
            _rigidbody.velocity = direction * MOVEMENT_SPEED * (deltaAngle > 30 ? 0.3f : 1);

            // Clamp angular velocity (e.g. after getting shot)
            _rigidbody.angularVelocity = Mathf.Sign(_rigidbody.angularVelocity) * Mathf.Min(Mathf.Sign(_rigidbody.angularVelocity), 1000);
        } else
        {
            _rigidbody.velocity *= 0.85f;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player") || /*XXX Temp test*/collision.gameObject.name == "Player")
        {
            GetComponent<AudioSource>().Play();
            collision.gameObject.GetComponent<Player>().KillThisGuy();
            FindObjectOfType<RoomGenerator>().OnRoomFailure();
        }
    }

}
