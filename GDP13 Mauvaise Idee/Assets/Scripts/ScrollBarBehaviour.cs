﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollBarBehaviour : MonoBehaviour {
    public UnityEngine.UI.Scrollbar scroll;

    public float taille=0.0f;//taille du scrollBar varie entre 0 et 1

	// Use this for initialization
	void Start () {
        scroll = gameObject.GetComponent<UnityEngine.UI.Scrollbar>();
        }

    // Update is called once per frame
    void Update() {
        scroll.size = taille;
        }
}