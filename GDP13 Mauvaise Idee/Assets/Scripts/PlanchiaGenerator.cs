﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanchiaGenerator : MonoBehaviour
{
    public int NbPlanches;
    public GameObject Planche;
    public float SpawnRadius;

    // Update is called once per frame
    void Update()
    {

    }

    public void CreatePlanches()
    {
        Vector3[] vecs = GetRandomPlacement();

        for (int i = 0; i < vecs.Length; i++)
        {
            if (vecs[i] != Vector3.zero)
            {
                var planche = Instantiate(Planche, transform);
                planche.transform.localPosition = vecs[i];
                planche.transform.Rotate(0, 0, Random.Range(0, 360));
            }
        }
    }

    public Vector3[] GetRandomPlacement()
    {
        Vector3[] vecArray = new Vector3[NbPlanches];
        for (int i = 0; i < NbPlanches; i++)
        {
            Vector3 vec = new Vector3();
            Vector3 choosenVectorWithSomeChuckNorrisSkills = Vector3.zero;
            bool placementIsEmpty = false;
            int nbTests = 0;

            while (!placementIsEmpty && nbTests < 200)
            {
                nbTests++;

                // todo ajouter robustesse à la taille de la map
                vec.x = Random.Range(-8f, 8f);
                vec.y = Random.Range(-4f, 4f);

                if (Physics2D.OverlapCircle(vec, SpawnRadius))
                {
                    placementIsEmpty = false;
                    choosenVectorWithSomeChuckNorrisSkills = Vector3.zero;
                }
                else
                {
                    placementIsEmpty = true;
                    choosenVectorWithSomeChuckNorrisSkills = vec;
                }
            }
            vecArray[i] = choosenVectorWithSomeChuckNorrisSkills;
        }
        return vecArray;
    }
}
