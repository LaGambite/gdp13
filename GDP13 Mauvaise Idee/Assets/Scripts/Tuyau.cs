﻿using Assets.Scripts.Interfaces;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tuyau : MonoBehaviour, IUsable {
    
    public static int tuyauxActivated = 0;

    [Range(1,3)]
    public int nbTuyaus = 1;
    public Sprite Cache;
    public Sprite Tuyau1;
    public Sprite Tuyau2;
    public Sprite Tuyau3;

    public PlatePosition position;

    public int expectedTuyauxActivated = 0;

    private SpriteRenderer _spriteRenderer;

    void Start () {
        _spriteRenderer = transform.Find("Cache").GetComponent<SpriteRenderer>();
        _spriteRenderer.sprite = Cache;
        tuyauxActivated = 0;
    }
	

	void Update () {
	}

    public bool Use(GameObject gameObject)
    {
        if (tuyauxActivated == expectedTuyauxActivated) {
            tuyauxActivated++;
            switch (nbTuyaus)
            {
                case (1):
                    _spriteRenderer.sprite = Tuyau1;
                    break;
                case (2):
                    _spriteRenderer.sprite = Tuyau2;
                    break;
                case (3):
                    _spriteRenderer.sprite = Tuyau3;
                    break;
            }
            return true;
        } else
        {
            GameState.currentRoomInstance.GetEntrance().Open();
            return false;
        }
    }
}