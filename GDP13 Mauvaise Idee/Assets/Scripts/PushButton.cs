﻿using Assets.Scripts.Interfaces;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PushButton : MonoBehaviour, IUsable
{
    
    public SpriteRenderer symbol;

    private Sprite sprite1;

    private Sprite sprite2;
    
    public void SetSymbol(Sprite symbolSprite)
    {
        symbol.sprite = symbolSprite;
    }

    public void SetExpectedCaisseSymbols(Sprite sprite1, Sprite sprite2)
    {
        this.sprite1 = sprite1;
        this.sprite2 = sprite2; /* peut être null */
    }

    public bool Use(GameObject gameObject)
    {
        if (gameObject.GetComponent<Caisse>() != null)
            if (gameObject.GetComponent<Caisse>().symbol.sprite == sprite1)
                GameState.currentRoomInstance.GetExit().Open();
            else
                GameState.currentRoomInstance.GetEntrance().Open();
        return true;
    }
}
