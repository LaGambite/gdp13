﻿using Assets.Scripts.Interfaces;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public float Acceleration = 1f;
    public KeyCode UseKey = KeyCode.F;
    public Sprite DeadPlayer;
    public GameObject indicator;

    private ITakeable _grabedObject;
    private bool isGrabingSomething;
    private Rigidbody2D _rigidbody;
    private GameObject CollidingObject;
    private bool isCollidingSomething;
    private bool playerIsAlive;

    // Use this for initialization
    void Start () {
        playerIsAlive = true;
        _rigidbody = this.GetComponent<Rigidbody2D>();
        indicator.SetActive(false);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (playerIsAlive)
        {
            if (CollidingObject != null)
            {
                indicator.transform.parent = CollidingObject.transform;
                indicator.transform.position = CollidingObject.transform.position;
            }

            // mouvement
            float h = Input.GetAxisRaw("Horizontal");
            float v = Input.GetAxisRaw("Vertical");
            Vector2 movement = new Vector2(h, v);

            _rigidbody.velocity += movement * Time.deltaTime * Acceleration;


            // mouvement souris
            var objectPos = Camera.main.WorldToScreenPoint(transform.position);
            var dir = Input.mousePosition - objectPos;

            var y = Input.GetAxis("Mouse Y"); //returns the Y-Axis and
            var x = Input.GetAxis("Mouse X");
            if (x != 0 || y != 0)
                transform.rotation = Quaternion.Euler(new Vector3(0, 0, Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg));

            float heading = Mathf.Atan2(movement.y, movement.x);
            transform.rotation = Quaternion.Euler(0f, 0f, heading * Mathf.Rad2Deg);

            if (Input.GetButtonDown("Use"))
            {
                if (isGrabingSomething && isCollidingSomething)
                {
                    TryToDropOnObject();
                }

                if (isGrabingSomething)
                {
                    TryToDrop();
                }

                else if (isCollidingSomething)
                {
                    TryToInteract();
                }
            }
        }
    }

    public void KillThisGuy()
    {
        playerIsAlive = false;
        GetComponent<CircleCollider2D>().enabled = false;
        GetComponent<AudioSource>().Play();
        GetComponent<SpriteRenderer>().sprite = DeadPlayer;
    }

    private void TryToInteract()
    {
        switch (CollidingObject.tag)
        {
            case ("Takeable"):
                TryToTake();
                break;
            case ("Usable"):
                TryToUse();
                break;
        }
    }

    private void TryToTake()
    {
        ITakeable takeable = CollidingObject.GetComponent<ITakeable>();

        if (takeable != null)
        if (takeable.Take())
            {
               _grabedObject = takeable;
                isGrabingSomething = true;
            }
    }

    private void TryToDrop()
    {
        if (_grabedObject.Drop())
        {
            _grabedObject = null;
            isGrabingSomething = false;
        }
    }

    private void TryToDropOnObject()
    {
        if (_grabedObject.DropOnObject(CollidingObject))
        {
            _grabedObject = null;
            isGrabingSomething = false;
        }
    }

    private void TryToUse()
    {
        CollidingObject.GetComponent<IUsable>().Use(this.gameObject);
    }

    // Gestion collisions
    private void OnTriggerEnter2D(Collider2D collision)
    {
        isCollidingSomething = true;
        CollidingObject = collision.gameObject;
        if (CollidingObject.CompareTag("Takeable") || CollidingObject.CompareTag("Usable"))
        {
            indicator.SetActive(true);
            indicator.transform.position = collision.transform.position;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject == CollidingObject)
        {
            isCollidingSomething = false;
            CollidingObject = null;
            indicator.SetActive(false);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
    }

}
