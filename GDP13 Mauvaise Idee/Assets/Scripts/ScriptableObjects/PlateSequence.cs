﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[CreateAssetMenu(fileName = "PlateSequence", menuName = "PlateSequence", order = 20)]
public class PlateSequence : ScriptableObject
{
    public List<PlatePosition> doorPositions;
}
