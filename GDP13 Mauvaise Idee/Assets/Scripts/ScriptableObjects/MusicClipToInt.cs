﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[CreateAssetMenu(fileName = "MusicClipToInt", menuName = "MusicClipToInt", order = 20)]
public class MusicClipToInt : ScriptableObject
{
	public AudioClip clip;

	public int value;

}
