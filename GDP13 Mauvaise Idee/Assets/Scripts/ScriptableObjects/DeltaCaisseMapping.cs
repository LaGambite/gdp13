﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[CreateAssetMenu(fileName = "DeltaCaisseMapping", menuName = "Delta Caisse Mapping", order = 10)]
public class DeltaCaisseMapping : ScriptableObject
{
    public Delta delta;

    public CaisseSymbol caisse1;

    public CaisseSymbol caisse2;

}
