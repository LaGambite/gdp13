﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[CreateAssetMenu(fileName = "PushButtonSymbol", menuName = "Push Button Symbol", order = 2)]
public class PushButtonSymbol : ScriptableObject
{

    [Header("Symbols")]

    public Sprite sprite;

    [Header("Omega")]

    public Omega omegaIfSame;

    public Omega omegaIfTwoRed;

    public Omega omegaIfDistinct;

    public Omega omegaElse;
    
}