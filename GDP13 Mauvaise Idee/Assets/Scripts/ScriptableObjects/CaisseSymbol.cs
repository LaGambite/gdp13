﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[CreateAssetMenu(fileName = "CaisseSymbol", menuName = "Caisse Symbol", order = 9)]
public class CaisseSymbol : ScriptableObject
{
    public Sprite sprite;
}
