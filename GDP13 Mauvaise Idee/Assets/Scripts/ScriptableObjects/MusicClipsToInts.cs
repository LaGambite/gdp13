﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[CreateAssetMenu(fileName = "MusicClipsToInts", menuName = "MusicClipsToInts", order = 20)]
public class MusicClipsToInts : ScriptableObject
{
	public List<MusicClipToInt> mappings;

	public int GetValue(AudioClip clip) {
		foreach (MusicClipToInt mapping in mappings) {
			if (mapping.clip.Equals (clip)) {
				return mapping.value;
			}
		}
		return 0;
	}

}
