﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[CreateAssetMenu(fileName = "DoorSequence", menuName = "DoorSequence", order = 20)]
public class DoorSequence : ScriptableObject
{

    public List<DoorPosition> doorPositions;

}
