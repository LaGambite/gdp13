﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[CreateAssetMenu(fileName = "RulesCaisses", menuName = "Rules Caisses", order = 0)]
public class RulesCaisses : ScriptableObject
{

    public List<PushButtonSymbol> pushButtonSymbolMappings;

    public List<OmegaDeltaMapping> omegaDeltaMappings;
    
    public List<CaisseSymbol> caisseSymbols;

    public List<DeltaCaisseMapping> deltaCaisseMappings;

    public Color redColor;

    public List<Color> otherColors;

    public Delta GetDelta(int caisseCount, Omega omega)
    {
        foreach (OmegaDeltaMapping odm in omegaDeltaMappings)
        {
            if (odm.caisseCount == caisseCount && odm.omega.Equals(omega))
            {
                return odm.delta;
            }
        }
        return Delta.D0;
    }

    public DeltaCaisseMapping GetDeltaCaisseMapping(Delta delta)
    {
        foreach (DeltaCaisseMapping dcm in deltaCaisseMappings)
        {
            if (dcm.delta.Equals(delta))
            {
                return dcm;
            }
        }
        return null;
    }

}
