﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[CreateAssetMenu(fileName = "PasswordMapping", menuName = "PasswordMapping", order = 20)]
public class PasswordMapping : ScriptableObject
{
    public PlatePosition mostPipesPlate;

    public PlatePosition leastPipesPlate;

    public string password;
}
