﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[CreateAssetMenu(fileName = "OmegaDeltaMapping", menuName = "Omega Delta Mapping", order = 4)]
public class OmegaDeltaMapping : ScriptableObject
{
    public int caisseCount;

    public Omega omega;

    public Delta delta;

}
