﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[CreateAssetMenu(fileName = "RulesTuyaux", menuName = "RulesTuyaux", order = 0)]
public class RulesTuyaux : ScriptableObject
{

    public DoorSequence doorSeqPair;

    public DoorSequence doorSeqImpair;

    public PlateSequence plateSeqImpair;

    public PlateSequence plateSeqPairImpair;

    public PlateSequence plateSeqPairPair;

    public List<PasswordMapping> passwordMappings;

    public PasswordMapping GetRandomPasswordMapping()
    {
        return passwordMappings[UnityEngine.Random.Range(0, passwordMappings.Count)];
    }

    public string GetPassword(PlatePosition mostPipesPosition, PlatePosition leastPipesPosition)
    {
        foreach (PasswordMapping passwordMapping in passwordMappings)
        {
            if (passwordMapping.mostPipesPlate == mostPipesPosition
                && passwordMapping.leastPipesPlate == leastPipesPosition)
            {
                return passwordMapping.password;
            }
        }
        return "";
    }
    
}
