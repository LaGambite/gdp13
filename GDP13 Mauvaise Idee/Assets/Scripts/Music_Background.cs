﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music_Background : MonoBehaviour {

	// Use this for initialization
	void Start () {
		AudioSource[] son = gameObject.GetComponents<AudioSource> ();

		son [0].Play (); //Musique
		son [1].Play (); //Souffle du perssonage
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
