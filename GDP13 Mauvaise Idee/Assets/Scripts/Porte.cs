﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Porte : MonoBehaviour {

    public const float POSITION_DURATION = 7f;

    public const float STAY_OPEN_TIME = 1f;

    public Quaternion openedRotation;

    public Quaternion closedRotation;

    public DoorPosition position;

    public DoorSequence sequence;

    private bool armed = false;

    private float lastChanged = 0;

    private float openingTime;

    private int id = -1;

    private bool isClosed = true;

    public void Arm(DoorSequence seq)
    {
        armed = true;
        lastChanged = Time.time;
        sequence = seq;
        Next();
    }

    public void Start()
    {
      
    }

    public void Update()
    {
        if (armed && Time.time - lastChanged > POSITION_DURATION)
        {
            Next();
            lastChanged = Time.time;
            openingTime = Time.time;
        }

        if(Time.time - openingTime > STAY_OPEN_TIME && !isClosed)
        {
            Close();
        }
    }

    private void Next() {
        id++;
        if (id > sequence.doorPositions.Count - 1) {
            id -= sequence.doorPositions.Count;
         }
        if (sequence.doorPositions[id] == position || sequence.doorPositions[id] == DoorPosition.BOTH)
            Open();
        else
            Close();
    }

    public void Open()
    {
        isClosed = false;
        GetComponent<SpriteRenderer>().color = new Color(0f, 128f, 0f, 1f);
        GetComponent<BoxCollider2D>().enabled = false;
        transform.rotation = openedRotation;
    }

    public void Close()
    {
        isClosed = true;
        GetComponent<SpriteRenderer>().color = new Color(128f, 0f, 0f, 1f);
        GetComponent<BoxCollider2D>().enabled = true;
        transform.rotation = closedRotation;
    }
}
