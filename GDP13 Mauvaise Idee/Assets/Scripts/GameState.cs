﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState {

    public static Room currentRoomInstance;

    public static bool isCurrentRoomInstanceCompleted;

    /**
     * Warning: holds the original room that is copy/pasted every time we play it.
     * Room contents must not be edited.
     */
    public static Room currentRoom;

    public static int score;

}
