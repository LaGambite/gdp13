﻿using Assets.Scripts.Interfaces;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Caisse : MonoBehaviour, ITakeable {

    public bool isGrabed = false;

    public SpriteRenderer symbol;

    public SpriteRenderer macaron;

    private SpriteRenderer _spriteRenderer;

    private int defaultSortingOrder;
    private int defaultMacaronSortingOrder;
    private int defaultSymbolSortingOrder;

    void Awake () {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        defaultSortingOrder = _spriteRenderer.sortingOrder;
        defaultMacaronSortingOrder = macaron.sortingOrder;
        defaultSymbolSortingOrder = symbol.sortingOrder;
    }
	
	void Update () {
		if (isGrabed)
        {
            ItemUtils.FollowObjectWithTag(transform, "CaisseLocation2");
        }
	}

    public bool Take()
    {
        isGrabed = true;
        _spriteRenderer.sortingOrder = 1200;
        macaron.sortingOrder = 1201;
        symbol.sortingOrder = 1202;
        GetComponent<BoxCollider2D>().enabled = false;
        return true;
    }

    public bool Drop()
    {
        isGrabed = false;
        _spriteRenderer.sortingOrder = defaultSortingOrder;
        macaron.sortingOrder = defaultMacaronSortingOrder;
        symbol.sortingOrder = defaultSymbolSortingOrder;
        GetComponent<BoxCollider2D>().enabled = true;
        return true;
    }

    public bool DropOnObject(GameObject gameObject)
    {
        if (gameObject.tag == "Usable")
        {
            if (gameObject.GetComponent<PushButton>() != null)
            {
               gameObject.GetComponent<IUsable>().Use(this.gameObject);
                transform.position = gameObject.transform.position;
                isGrabed = false;
                return true;
            }
        }
        return false;
    }

    public void SetColor(Color color)
    {
        macaron.color = color;
    }

    public void SetSymbol(Sprite symbolSprite)
    {
        symbol.sprite = symbolSprite;
    }

    public Sprite GetSymbol()
    {
        return symbol.sprite;
    }
}
