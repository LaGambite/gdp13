﻿using Assets.Scripts.Interfaces;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Levier : MonoBehaviour, IUsable {
    
	public int valeur_levier;

    public GameObject activeLever;

    public GameObject inactiveLever;

	public int solution;

    void Start()
    {
        activeLever = transform.Find("Levier Actif").gameObject;
        inactiveLever = transform.Find("Levier Inactif").gameObject;

        activeLever.SetActive(false);
        inactiveLever.SetActive(true);
    }

    public bool Use(GameObject gameObject)
    {
		Levier_Generator levier_gen = GameObject.Find("RoomMusique").GetComponent<Levier_Generator>();

		if (valeur_levier == solution && levier_gen.is_use == false) {
			levier_gen.is_use = true;
			
			Exit Door_sortie = GameObject.Find ("Exit").GetComponent<Exit> ();
			GameState.currentRoomInstance.GetExit().Open();

            activeLever.SetActive(true);
            inactiveLever.SetActive(false);

			//Sound Design
			this.gameObject.GetComponent<AudioSource>().Play();

            return false;
        }

		if (valeur_levier != solution && levier_gen.is_use == false) {
			levier_gen.is_use = true;
			
			Entrance Door_entree = GameObject.Find ("Entrance").GetComponent<Entrance> ();
			GameState.currentRoomInstance.GetEntrance().Open();

            activeLever.SetActive(true);
            inactiveLever.SetActive(false);

			//Sound Design
			this.gameObject.GetComponent<AudioSource>().Play();

            return false;

        } else {
			return false;
		}
    }
}

