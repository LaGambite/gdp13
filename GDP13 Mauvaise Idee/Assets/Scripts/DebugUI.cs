﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugUI : MonoBehaviour {

    public void OpenEntrance()
    {
        GameState.currentRoomInstance.GetEntrance().Open();
    }

    public void OpenExit()
    {
        GameState.currentRoomInstance.GetExit().Open();
    }

}
