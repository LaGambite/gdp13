﻿using Assets.Scripts.Interfaces;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PasswordTextBox : MonoBehaviour, IUsable
{
    public string stringToEdit = "Hello World";
    public SubmitPassword consoleBox;

    public string rightPassword = "lol";

    void Start()
    {
        consoleBox.gameObject.SetActive(false);
    }
    
    public bool Use(GameObject gameObject)
    {
        consoleBox.passwordTextBox = this;
        consoleBox.gameObject.SetActive(true);
        return true;
    } 

    public void TestPassword(string password)
    {
        if (password.Trim().Length > 0)
        {
            if (password.Contains(rightPassword))
                GameState.currentRoomInstance.GetExit().Open();
            else
                GameState.currentRoomInstance.GetEntrance().Open();
        }
        consoleBox.gameObject.SetActive(false);
    }
}