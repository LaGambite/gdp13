﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class music_box : MonoBehaviour {
	
	public Collider2D coll_music;
	AudioSource mon_son;
	public AudioClip[] all_song;
	public MusicClipsToInts mappingMusic;
	public int value;

	// Use this for initialization

	public void Init(){
		mon_son = gameObject.GetComponent<AudioSource> ();
		mon_son.clip = all_song[Random.Range (0, all_song.Length)];
		value = mappingMusic.GetValue (mon_son.clip);
	}
	// Update when something touch me
	void OnTriggerEnter2D(Collider2D collision){
		mon_son.Play ();
		}
	void OnTriggerExit2D(Collider2D collision){
		mon_son.Stop ();
	}


	}
