﻿using Assets.Scripts.Interfaces;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entrance : MonoBehaviour, IUsable {

    public float temps;
    float tempsInit;
    private float tempsMax;
    public ScrollBarBehaviour scrollBar;

    private GameObject collisions;
    private GameObject door;

    public bool armed = false;
    private bool open = false;
    
    void Awake()
    {
        collisions = transform.Find("Collisions").gameObject;
        door = transform.Find("Porte").gameObject;

        tempsInit = 0;
        scrollBar = FindObjectOfType<ScrollBarBehaviour>();

		gameObject.GetComponent<AudioSource> ().Play ();
    }
    
    void Update()
	{
		if (armed && tempsInit == 0) {
			tempsInit = Time.time;
		}

		{
			if (armed) {
				temps = Time.time - tempsInit;
				if (temps > tempsMax) {
					Open ();
				}

				afficherTemps ();

			}
		}

		//Sound Design
		if (open == true)
			gameObject.GetComponent<AudioSource> ().Stop ();
	
	}
    public bool Use(GameObject gameObject)
    {
        GameObject.Find("DoorSongManager").GetComponent<AudioSource>().Play();
        ajouterTemps(20);
        return true;
    }
    
    void afficherTemps()
    {
        if (scrollBar == null)
        {
            scrollBar = FindObjectOfType<ScrollBarBehaviour>();
        }
        scrollBar.taille = 1 - temps / tempsMax;
    }

    public void ajouterTemps(int deltaTemps)//si delta temps est positif cela ajoute du temps pour le joueur
    {
        //deltaTemps en secondes
        if (tempsInit + deltaTemps >= tempsMax)
            tempsInit = Time.time;
        else
            tempsInit += deltaTemps;
    }

    public void Arm(float tempsMax)
    {
        this.tempsMax = tempsMax;
        if (scrollBar != null)
        {
            scrollBar.gameObject.SetActive(true);
        }
        armed = true;
    }

    public void Open()
    {
        if (!open)
        {
            Destroy(collisions);
            Destroy(door);
            if (scrollBar != null)
            {
                scrollBar.gameObject.SetActive(false);
            }
            open = true;
        }
    }

    public void OnDestroy()
    {
        if (scrollBar != null)
        {
            scrollBar.gameObject.SetActive(true);
        }
    }
}
