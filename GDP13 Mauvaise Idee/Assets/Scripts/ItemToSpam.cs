﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemToSpam : MonoBehaviour
{
    public const float TIME_FOR_SPAM = 15f;
    public const int NB_SPAWN_NEEDED = 1000;
    private float StartSpamingTime;
    private int nbSpam = 0;
    private bool _isBeenSpammed;
    public GameObject attachedGameObject;

    public void Start()
    {
        _isBeenSpammed = false;
    }

    public void Update()
    {
        if(_isBeenSpammed)
        {
            if (Input.GetButtonDown("Use"))
            {
                if (_isBeenSpammed)
                {
                    if (StartSpamingTime + TIME_FOR_SPAM > Time.time)
                    {
                        if (nbSpam > NB_SPAWN_NEEDED)
                            GetThemAll();
                        nbSpam++;
                    }
                    else
                    {
                        // on a depassé le temps limite
                        nbSpam = 0;
                        _isBeenSpammed = false;
                    }
                }
            }
        }
    }
    public void GetThemAll()
    {
        attachedGameObject = null;
        _isBeenSpammed = false;
    }


    public void Use(GameObject gameObject)
    {
        attachedGameObject = gameObject;
        _isBeenSpammed = true;
        nbSpam++;
        StartSpamingTime = Time.time;
    }

}
