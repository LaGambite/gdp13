﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class CaisseGenerator : MonoBehaviour
{
    private List<Caisse> _caisses = new List<Caisse>();

    private PushButton _pushButton;

    void Awake()
    {
        foreach (Transform child in transform.Find("Contents")) { 
            if (child.GetComponent<Caisse>() != null)
            {
                _caisses.Add(child.GetComponent<Caisse>());
            } else if (child.GetComponent<PushButton>() != null)
            {
                _pushButton = child.GetComponent<PushButton>();
            }
        }
    }

    public void Generate(RulesCaisses rules)
    {
        if (_pushButton == null || _caisses.Count < 2)
        {
            return;
        }

        // Choose push button symbol
        PushButtonSymbol pbs = rules.pushButtonSymbolMappings[
            UnityEngine.Random.Range(0, rules.pushButtonSymbolMappings.Count)];
        if (pbs == null || _pushButton == null) return;
        _pushButton.SetSymbol(pbs.sprite);

        // Choose caisses omega, compute delta
        int n = UnityEngine.Random.Range(0, 4);
        Omega omega;
        switch (n)
        {
            case 0: omega = pbs.omegaIfSame; break;
            case 1: omega = pbs.omegaIfTwoRed; break;
            case 2: omega = pbs.omegaIfDistinct; break;
            default: omega = pbs.omegaElse; break;
        }
        int caisseCount = _caisses.Count;
        Delta delta = rules.GetDelta(caisseCount, omega);
        DeltaCaisseMapping deltaCaisseMapping = rules.GetDeltaCaisseMapping(delta);
        // todo rajouter la compatibilité pour le jeu avec 2 symboles
        _pushButton.SetExpectedCaisseSymbols(deltaCaisseMapping.caisse1.sprite, null);

        // Choose colors
        List<Color> caisseColors = new List<Color>();
        List<Color> remainingColors = new List<Color>(rules.otherColors);
        remainingColors.Add(rules.redColor);
        if (n == 0) // All same
        {
            if (caisseCount == 2)
            {
                // Avoid Omega B
                remainingColors.Remove(rules.redColor);
            }
            Color uniqueColor = GetRandomColor(remainingColors);
            foreach (Caisse caisse in _caisses) caisseColors.Add(uniqueColor);
        } else if (n == 1) // 2 red
        {
            caisseColors.Add(rules.redColor);
            caisseColors.Add(rules.redColor);

            remainingColors.Remove(rules.redColor);
            for (int i = 0; i < _caisses.Count - 2; i++)
            {
                caisseColors.Add(GetRandomColor(remainingColors));
            }
        }
        else if (n == 2) // Distinct
        {
            for (int i = 0; i < _caisses.Count; i++)
            {
                caisseColors.Add(GetRandomColor(remainingColors, true));
            }
        }
        else // Other (always 2 same + distinct)
        {
            remainingColors.Remove(rules.redColor);
            Color doubleColor = GetRandomColor(remainingColors, true);
            caisseColors.Add(doubleColor);
            caisseColors.Add(doubleColor);

            remainingColors.Add(rules.redColor);
            for (int i = 0; i < _caisses.Count - 2; i++)
            {
                caisseColors.Add(GetRandomColor(remainingColors, true));
            }
        }

        // Choose symbols
        List<Sprite> caisseSymbols = new List<Sprite>();
        List<CaisseSymbol> remainingSymbols = new List<CaisseSymbol>(rules.caisseSymbols);
        caisseSymbols.Add(deltaCaisseMapping.caisse1.sprite);
        remainingSymbols.Remove(deltaCaisseMapping.caisse1);
        if (deltaCaisseMapping.caisse2)
        {
            caisseSymbols.Add(deltaCaisseMapping.caisse2.sprite);
            remainingSymbols.Remove(deltaCaisseMapping.caisse2);
        }
        int remaining = _caisses.Count - caisseSymbols.Count;
        for (int i = 0; i < remaining; i++)
        {
            CaisseSymbol symbol = remainingSymbols[UnityEngine.Random.Range(0, remainingSymbols.Count)];
            caisseSymbols.Add(symbol.sprite);
            remainingSymbols.Remove(symbol);
        }

        // Apply colors
        Shuffle(caisseColors);
        Shuffle(caisseSymbols);
        for (int i = 0; i < _caisses.Count; i++)
        {
            Caisse caisse = _caisses[i];
            caisse.SetColor(caisseColors[i]);
            caisse.SetSymbol(caisseSymbols[i]);
        }
    }

    public Color GetRandomColor(List<Color> colors, bool remove = false)
    {
        Color c = colors[UnityEngine.Random.Range(0, colors.Count)];
        if (remove) colors.Remove(c);
        return c;
    }

    private void Shuffle<T>(IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = UnityEngine.Random.Range(0, n);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

}