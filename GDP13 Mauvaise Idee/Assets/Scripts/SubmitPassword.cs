﻿using Assets.Scripts.Interfaces;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SubmitPassword : MonoBehaviour 
{
   
    public PasswordTextBox passwordTextBox = null;
    
    void Update()
    {
        if (passwordTextBox != null && Input.GetKeyDown(KeyCode.Return))
        {
            passwordTextBox.TestPassword(GetComponent<InputField>().text);
        }
    }
}