﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public void Target(Transform target)
    {
        transform.position = new Vector3(target.position.x, target.position.y, transform.position.z);
    }
}
