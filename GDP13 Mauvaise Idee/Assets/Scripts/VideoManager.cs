﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class VideoManager : MonoBehaviour {

    private VideoPlayer videoPlayer;

	// Use this for initialization
	void Start () {
        videoPlayer = GetComponent<VideoPlayer>();
        videoPlayer.loopPointReached += EndReached;
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Escape))
        {
            EndReached(null);
        }
	}
    private void EndReached(VideoPlayer vp)
    {
        SceneManager.LoadScene("Scenes/Game");
    }
}
