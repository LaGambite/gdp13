﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Interfaces
{
    interface IUsable
    {
        bool Use(GameObject gameObject);
    }
}
