﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class TuyauGenerator : MonoBehaviour { 

    private List<Tuyau> _tuyaux = new List<Tuyau>();

    private MurCentre _murCentre;

    private PasswordTextBox _passwordTextBox;

    private Porte _doorSouth;

    private Porte _doorNorth;

    void Awake()
    {
        foreach (Transform child in transform.Find("Contents")) { 
            if (child.GetComponent<Tuyau>() != null)
            {
                _tuyaux.Add(child.GetComponent<Tuyau>());
            } else if (child.GetComponent<MurCentre>() != null)
            {
                _murCentre = child.GetComponent<MurCentre>();
            }
            else if (child.GetComponent<PasswordTextBox>() != null)
            {
                _passwordTextBox = child.GetComponent<PasswordTextBox>();
            } else if (child.GetComponent<Porte>() != null)
            {
                Porte porte = child.GetComponent<Porte>();
                if (porte.position == DoorPosition.NORTH)
                {
                    _doorNorth = porte;
                } else
                {
                    _doorSouth = porte;
                }
            }
        }
    }

    public void Generate(RulesTuyaux rules)
    {
        if (_passwordTextBox == null)
        {
            return;
        }

        // Center number
        int centerNumber =UnityEngine.Random.Range(10, 100);
        _murCentre.SetText(centerNumber.ToString());

        bool centerNumberEven = centerNumber % 2 == 0;
        bool centerFirstDigitEven = Mathf.Floor(centerNumber/10) % 2 == 0;

        // Door seqs
        _doorSouth.Arm(centerNumberEven ? rules.doorSeqPair : rules.doorSeqImpair);
        _doorNorth.Arm(centerNumberEven ? rules.doorSeqPair : rules.doorSeqImpair);

        // Plate seqs
        List<PlatePosition> plateSeq;
        if (!centerNumberEven)
        {
            plateSeq = rules.plateSeqImpair.doorPositions;
        } else
        {
            if (centerFirstDigitEven)
            {
                plateSeq = rules.plateSeqPairPair.doorPositions;
            } else
            {
                plateSeq = rules.plateSeqPairImpair.doorPositions;
            }
        }
        foreach (Tuyau tuyau in _tuyaux)
        {
            tuyau.expectedTuyauxActivated = plateSeq.IndexOf(tuyau.position);
        }

        // Password
        PasswordMapping pm = rules.GetRandomPasswordMapping();
        foreach (Tuyau tuyau in _tuyaux)
        {
            if (tuyau.position == pm.leastPipesPlate) tuyau.nbTuyaus = 1;
            else if (tuyau.position == pm.mostPipesPlate) tuyau.nbTuyaus = 3;
            else tuyau.nbTuyaus = 2;
        }
        _passwordTextBox.rightPassword = pm.password;
    }

}