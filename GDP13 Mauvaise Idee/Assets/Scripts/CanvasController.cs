﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasController : MonoBehaviour {

    public const float FADE_DURATION = 0.3f;

    public Image black;

    private Action action = null;

    private float targetOpacity;

    private float velocityOpacity = 0;

    void Start () {
        targetOpacity = black.color.a;
        black.gameObject.SetActive(true);
	}
	
	void Update () {
        if (black.color.a != targetOpacity)
        {
            black.color = new Color(0, 0, 0, Mathf.SmoothDamp(black.color.a, targetOpacity, ref velocityOpacity, FADE_DURATION));
            if (Mathf.Abs(black.color.a - targetOpacity) < 0.01f) black.color = new Color(0, 0, 0, targetOpacity);
        } else if (action != null)
        {
            action.Invoke();
            action = null;
        }
	}

    public void FadeOut(Action action = null)
    {
        SetTargetOpacity(1, action);
    }

    public void FadeIn(Action action = null)
    {
        SetTargetOpacity(0, action);
    }

    private void SetTargetOpacity(float opacity, Action action = null) {
        if (black.color.a != opacity)
        {
            targetOpacity = opacity;
            velocityOpacity = 0;
            this.action = action;
        }
        else if (action != null)
        {
            action.Invoke();
        }
    }
}
