﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour {
    
    private Entrance _entrance;

    private Exit _exit;

    public float tempsInitial = 30;

	void Awake ()
    {
        Transform tEntrance = transform.Find("Entrance");
        if (tEntrance == null) throw new System.Exception("Room " + gameObject.name + " has no entrance");
        _entrance = tEntrance.GetComponent<Entrance>();
        
        Transform tExit = transform.Find("Exit");
        if (tExit == null) throw new System.Exception("Room " + gameObject.name + " has no exit");
        _exit = tExit.GetComponent<Exit>();


	}

    internal void Init()
    {
        _entrance.Arm(tempsInitial);
		GetComponent<PlanchiaGenerator>().CreatePlanches();
		GetComponent<Levier_Generator>().Init();
    }

    public Entrance GetEntrance()
    {
        return _entrance;
    }

    public Exit GetExit()
    {
        return _exit;
    }

}