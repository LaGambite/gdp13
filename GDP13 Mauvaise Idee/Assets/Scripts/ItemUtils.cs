﻿using Assets.Scripts.Interfaces;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class ItemUtils
{
    
    public static bool FollowObjectWithTag(Transform transform, string tag)
    {
        GameObject target = GameObject.FindWithTag(tag);
        if (target != null)
        {
            transform.position = target.transform.position;
            return true;
        } else
        {
            return false;
        }
    }

}