﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RoomGenerator : MonoBehaviour
{

    public Player playerPrefab;

    public Enemy enemyPrefab;

    public new CameraController camera;

    public CanvasController canvas;

    public RulesCaisses rulesCaisses;

    public RulesTuyaux rulesTuyaux;

    [Header("Debug settings")]
    public bool forceTestRoom;
    public Room testRoom;

    private List<Room> _rooms = new List<Room>();

    private List<Room> _completedRooms = new List<Room>();

    private GameObject _roomInstanceObj;

    void Start () {
        for (int i = 0; i < transform.childCount; i++) {
            Transform child = transform.GetChild(i);
            Room room = child.GetComponent<Room>();
            if (room != null)
            {
                _rooms.Add(room);
            }
        }

        if (forceTestRoom)
        {
            if (testRoom == null) throw new System.Exception("No test room selected");
            StartRoom(testRoom);
        }
        else
        {
            StartRandomRoom();
        }
    }

    public void OnRoomFailure()
    {
        if (!GameState.isCurrentRoomInstanceCompleted) {
            GameState.isCurrentRoomInstanceCompleted = true;
            canvas.FadeOut(() => {
                StartRoom(GameState.currentRoom);
            });
        }
        canvas.FadeOut(() => {
            SceneManager.LoadScene("Scenes/RestartMenu");
        });
    }

    public void OnRoomVictory()
    {
        if (!GameState.isCurrentRoomInstanceCompleted)
        {
            GameState.isCurrentRoomInstanceCompleted = true;
            GameState.score++;
            _completedRooms.Add(GameState.currentRoom);
            canvas.FadeOut(() => {
                if (GameState.score >= 3)
                {
                    SceneManager.LoadScene("Scenes/EndingMenu");
                }
                else
                {
                    StartRandomRoom();
                }
            });
        }
    }

    public void StartRandomRoom()
    {
        if (_completedRooms.Count < _rooms.Count)
        {
            Room room;
            int attempts = 0;
            do
            {
                room = _rooms[Random.Range(0, _rooms.Count)];
            } while (_completedRooms.Contains(room) && attempts++ < 50);
            StartRoom(room);
        }
        else
        {
            StartRoom(_rooms[Random.Range(0, _rooms.Count)]);
        }
    }

    public void StartRoom(Room room)
    {
        GameState.currentRoom = room;
        GameState.isCurrentRoomInstanceCompleted = false;

        if (_roomInstanceObj != null)
        {
            Destroy(_roomInstanceObj);
        }
        _roomInstanceObj = Instantiate(room.gameObject, new Vector3(10000, 0), Quaternion.identity);
        _roomInstanceObj.name = "RoomInstance";
        Room roomInstance = _roomInstanceObj.GetComponent<Room>();
        roomInstance.Init();
        GameState.currentRoomInstance = roomInstance;

        _roomInstanceObj.GetComponent<CaisseGenerator>().Generate(rulesCaisses);
        _roomInstanceObj.GetComponent<TuyauGenerator>().Generate(rulesTuyaux);

        GameObject playerObj = Instantiate(playerPrefab.gameObject, roomInstance.transform);
        playerObj.transform.position = roomInstance.GetEntrance().transform.position + new Vector3(1, 0);
        playerObj.name = "Player";

        GameObject enemyObj = Instantiate(enemyPrefab.gameObject, roomInstance.transform);
        enemyObj.transform.position = roomInstance.GetEntrance().transform.position - new Vector3(2, 0);
        enemyObj.name = "Enemy";

        camera.Target(roomInstance.transform);
        canvas.FadeIn();
    }

	void Update(){
		//CheatCode
		if (Input.GetKeyDown(KeyCode.F12))
			GameState.currentRoomInstance.GetExit().Open();
        if (Input.GetKeyDown(KeyCode.F11))
            GameState.currentRoomInstance.GetEntrance().Open();
    }
}
