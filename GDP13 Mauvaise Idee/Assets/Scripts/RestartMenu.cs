﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class RestartMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKey(KeyCode.A))
        {
            Restart();
        }
	}

    public void Restart()
    {
        SceneManager.LoadScene("Scenes/Game");
    }

    public void Close()
    {
        Application.Quit();
    }
}
