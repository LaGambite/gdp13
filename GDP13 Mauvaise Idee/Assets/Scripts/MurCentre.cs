﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MurCentre : MonoBehaviour {

    public TextMesh textMesh;
    
    public void SetText(string text)
    {
        textMesh.text = text;
    }
}
