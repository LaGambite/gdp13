﻿using Assets.Scripts.Interfaces;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planche : MonoBehaviour, ITakeable {

    public bool isGrabed = false;

    private SpriteRenderer _spriteRenderer;

    private int defaultSortingOrder;

    void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        defaultSortingOrder = _spriteRenderer.sortingOrder;
    }
    
    void Update()
    {
        if (isGrabed)
        {
            ItemUtils.FollowObjectWithTag(transform, "CaisseLocation2");
        }
    }
        public bool Take()
    {
        isGrabed = true;
        _spriteRenderer.sortingOrder = 1200;
        GetComponent<BoxCollider2D>().enabled = false;
        return true;
    }

    public bool Drop()
    {
        isGrabed = false;
        _spriteRenderer.sortingOrder = defaultSortingOrder;
        GetComponent<BoxCollider2D>().enabled = true;
        return true;
    }

    public bool DropOnObject(GameObject gameObject)
    {
        if (gameObject.tag == "Entrance")
        {
                gameObject.GetComponent<IUsable>().Use( this.gameObject); 
                Destroy(this.gameObject);
                return true;
            
        }
        return false;
    }
}