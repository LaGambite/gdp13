﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Exit : MonoBehaviour {

    private GameObject collisions;
    private GameObject door;


    private bool open = false;

    public void Start()
    {
        collisions = transform.Find("Collisions").gameObject;
        door = transform.Find("Porte").gameObject;
    }

    public void Open()
    {
        if (!open)
        {
            open = true;
            Destroy(collisions);
            Destroy(door);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") || /*XXX Temp test*/collision.gameObject.name == "Player")
        {
            FindObjectOfType<RoomGenerator>().OnRoomVictory();
        }
    }

}
