﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Levier_Generator : MonoBehaviour {

	public bool is_use = false;

	private int solution = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Init() {
		music_box[] boxes = transform.Find ("Contents").GetComponentsInChildren<music_box> ();

		solution = 0;
		foreach (music_box box in boxes) {
			box.Init ();
			solution += box.value;
		}

		GenerateValuesForLeviers ();
	}

	public void GenerateValuesForLeviers()
	{
		int randomValue1 = Random.Range (0, 15);
		int randomValue2 = Random.Range (0, 15);

		while(randomValue1 == solution)
			randomValue1 = Random.Range(0, 15);

		while(randomValue2 == solution || randomValue2 == randomValue1)
			randomValue2 = Random.Range(0, 15);

		Levier[] x = this.gameObject.GetComponentsInChildren<Levier>();
		List<int> values = new List<int>() {solution,randomValue1,randomValue2};
		for (int i = 0; i < x.Length; i++) 
		{
			var randomInt = Random.Range (0, values.Count);
			//Donne une valeur aléatoire a chaque levier
			x [i].valeur_levier = values [randomInt];
			x [i].solution = solution;

			//Affiche la valeur du levier sur son GUI text
			x[i].GetComponentInChildren<TextMesh>().text = x[i].valeur_levier.ToString();
		
			//Supprime le levier de la liste
			values.Remove(values[randomInt]);
		}
	}
}
